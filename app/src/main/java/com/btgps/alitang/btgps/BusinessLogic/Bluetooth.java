package com.btgps.alitang.btgps.BusinessLogic;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.Set;

/**
 * Created by Wei Tang on 2015-09-13.
 */
public class Bluetooth {
    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    public BluetoothAdapter btAdaptor = null;
    private static Bluetooth bluetoothObj = null;

    public static Bluetooth make() {
        if(bluetoothObj != null)
            return bluetoothObj;
        return new Bluetooth();
    }

    public Bluetooth(){
        IsDeviceSupported();
    }

    public boolean equals() {
        return false;
    }

    public boolean IsDeviceSupported(){
        //if bt adaptor is null then try to get the default adaptor
        btAdaptor = BluetoothAdapter.getDefaultAdapter();

        //check if the adaptor is still bluetooth adaptor
        //is still null
        if(btAdaptor != null)
            return true;
        return false;
    }

    public boolean IsEnabled(){
        return btAdaptor.isEnabled();
    }

    public Intent BtEnableRequest(){
        return new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
    }

    public BluetoothDevice ConnectDevice(String btMacAddr, boolean isSecure){
        if(!IsEnabled())
            return null;

        return btAdaptor.getRemoteDevice(btMacAddr);
    }

    public Set<BluetoothDevice> GetBoundedDevices(){
        if(!IsEnabled())
            return null;
        return btAdaptor.getBondedDevices();
    }

    public void SearchDevice(){
        if(btAdaptor.isDiscovering())
            btAdaptor.cancelDiscovery();
        btAdaptor.startDiscovery();
    }

    public boolean IsDiscovering(){
        return btAdaptor.isDiscovering();
    }

    public void CancelDiscovery(){
        if(btAdaptor != null)
            btAdaptor.cancelDiscovery();
    }

    public BluetoothDevice DeviceFound(Intent curIntent){
        BluetoothDevice btdevice = curIntent
                .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        boolean isBounded = btdevice.getBondState() == BluetoothDevice.BOND_BONDED;

        if(!isBounded)
            return btdevice;

        return null;
    }

    public IntentFilter RegDeviceDiscovery(){
        return new IntentFilter(BluetoothDevice.ACTION_FOUND);
    }

    public IntentFilter RegDeviceDiscoveryFinish(){
        return new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
    }
}
