package com.btgps.alitang.btgps.Activities.NavigationDrawer;

/**
 * Created by aliTang on 11/11/15.
 */

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.btgps.alitang.btgps.Activities.Activities_Transmit.GpsTransmit;
import com.btgps.alitang.btgps.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public static PlaceholderFragment placeholderFragment = null;

    //make function
    public static PlaceholderFragment make(){
        if(placeholderFragment != null){
           return placeholderFragment = new PlaceholderFragment();
        }
        return placeholderFragment;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((GpsTransmit) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}
