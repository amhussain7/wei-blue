package com.btgps.alitang.btgps.BusinessLogic.BTServerFiles;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import com.btgps.alitang.btgps.BusinessLogic.Bluetooth;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by aliTang on 10/21/15.
 */
public class AcceptThread {
   /* private BluetoothServerSocket BTServerSocket;
    private BluetoothDevice BTDevice;
    private String connSocketType;
    private static final String TAG = "BTAcceptThread";

    public AcceptThread(Handler sHandler, boolean isSecure){


        //get a BT Socket for a presented device
        if(isSecure){
            connSocketType = "Secure";
            GetSetBTSocket(SECURE_UUID);}
        else{
            connSocketType = "InSecure";
            GetSetBTSocket(INSURE_UUID);}

    }

    public void GetSetBTSocket( UUID uuid){
        BluetoothServerSocket BTSocketTmp = null;
        try {
            BTSocketTmp = Bluetooth.make().btAdaptor.
                    listenUsingInsecureRfcommWithServiceRecord(SECURE_NAME, SECURE_UUID);
        } catch (IOException ex){
            Log.e(TAG, "Socket Type: " + connSocketType + "create() failed", ex);
        }
        BTServerSocket = BTSocketTmp;
    }

    @Override
    public void run() {
        Log.d(TAG, "Socket Type: " + connSocketType + "BEGIN mAcceptThread" + this);
        // setName("AcceptThread" + connSocketType);

        BluetoothSocket socketBT = null;

        if(BTServerSocket == null)
            return;

        //keep listening to BT Server if we are not connected
        while (sState != STATE_CONNECTED) {
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                socketBT = BTServerSocket.accept();
            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + connSocketType + "accept() failed", e);
                break;
            }

            //if BT connection accepted
            if(socketBT != null)
                synchronized (BTServerSocket){
                    switch (sState) {
                        //if idle or already connected
                        case STATE_IDLE:
                        case STATE_CONNECTED:
                            try{
                                socketBT.close();
                            }catch (IOException ex){
                                Log.e(TAG, "Unable to close the socket", ex);
                            }
                            break;
                        //this went well, call connected thread
                        case STATE_HEARING:
                        case STATE_CONNECTING:
                            Connected(socketBT, connSocketType);
                            break;
                    }
                }
        }
        Log.i(TAG, "END AcceptSecure Thread, socket Type: " + connSocketType);
    }

    public void cancel() {
        Log.d(TAG, "Socket Type" + connSocketType + "cancel " + this);
        try {
            BTServerSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Socket Type" + connSocketType + "close() of server failed", e);
        }
    }*/
}
