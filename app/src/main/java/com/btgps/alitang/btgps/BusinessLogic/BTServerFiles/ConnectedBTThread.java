package com.btgps.alitang.btgps.BusinessLogic.BTServerFiles;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.provider.SyncStateContract;
import android.util.Log;

import com.btgps.alitang.btgps.BusinessLogic.Bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;


/**
 * Created by aliTang on 10/21/15.
 */
public class ConnectedBTThread {
    /*private BluetoothSocket socketBT;
    private final InputStream inputStreamBT;
    private final OutputStream outputStreamBT;
    private String connSocketType;
    private static final String TAG = "BTConnectedThread";


    public ConnectedBTThread(Handler sHandler, BluetoothSocket btSocket, String connSocketType){


        socketBT = btSocket;
        InputStream inputStreamBTTemp = null;
        OutputStream outputStreamBTTemp = null;

        //get a BT Socket for a input and output stream
        try {
            inputStreamBTTemp = socketBT.getInputStream();
            outputStreamBTTemp = socketBT.getOutputStream();
        } catch (IOException ex){
            Log.e(TAG, "sockets are not created", ex);
        }
        inputStreamBT = inputStreamBTTemp;
        outputStreamBT = outputStreamBTTemp;
    }

    @Override
    public void run() {
        Log.d(TAG, "Start this Thread");
        // setName("AcceptThread" + connSocketType);
        byte[] dataBuffer = new byte[1024];
        int byteCout = 0;

        while (true){
        //Start listening the inputstream while connected
        try{
            //read buffer from input stream
            byteCout = inputStreamBT.read(dataBuffer);

            //send this data to the UI through handler
            sHandler.obtainMessage(Bluetooth.MESSAGE_READ,byteCout, -1, dataBuffer).sendToTarget();
        }catch (IOException ex){
            Log.e(TAG, "disconnected", ex);
            ResetServer("BTConnLost: Device connection was lost");
            // Start the service over to restart listening mode
            //StartService();
            break;}
        }
    }

    /**
     * Write to the connected OutStream.
     *
     * @param buffer The bytes to write
     */
    /*public void write(byte[] buffer) {
        try {
            outputStreamBT.write(buffer);

            // Share the sent message back to the UI Activity
            sHandler.obtainMessage(Bluetooth.MESSAGE_WRITE, -1, -1, buffer)
                    .sendToTarget();
        } catch (IOException ex) {
            Log.e(TAG, "Exception during write", ex);
        }
    }

    public void cancel() {
        try {
            socketBT.close();
        } catch (IOException ex) {
            Log.e(TAG, "close() of connect socket failed", ex);
        }
    }*/
}
