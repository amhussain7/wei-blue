package com.btgps.alitang.btgps.BusinessLogic.LatLong;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by aliTang on 10/27/15.
 */
    public class GpsData extends Service {
    private LocationManager bLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 1f;
    private static final String TAG = "GPS LISTENER";
    private MyLocationListener[] bLocationListeners = new MyLocationListener[]{
            new MyLocationListener(LocationManager.GPS_PROVIDER),
            new MyLocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }
    @Override
    public void onCreate()
    {
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
            bLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    bLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "Location update, Failed", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "No Network Provider available, " + ex.getMessage());
        }
        try {
            bLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    bLocationListeners[0]);
        } catch (SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy()
    {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (bLocationManager != null) {
            for (LocationListener listener :  bLocationListeners) {
                try {
                    bLocationManager.removeUpdates(listener);
                } catch (Exception ex) {
                    Log.i(TAG, "LOCATION LISTENER CANNOT BE REMOVED AT THIS TIME", ex);
                }
            }
        }
    }
    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (bLocationManager == null) {
            bLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    public class MyLocationListener implements LocationListener {
            Location blocation;


            public MyLocationListener(String provider) {
                Log.e(TAG, "Location Listener" + provider);
                blocation = new Location(provider);
            }

            @Override
            public void onLocationChanged(Location location) {
                Log.e(TAG, "onLocationChanged: " + location);
                blocation.set(location);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                Log.e(TAG, "onProviderDisabled: " + s);
            }

            @Override
            public void onProviderEnabled(String s) {
                Log.e(TAG, "onProviderEnabled: " + s);
            }

            @Override
            public void onProviderDisabled(String s) {
                Log.e(TAG, "onProviderDisabled: " + s);
            }


        }
}
