package com.btgps.alitang.btgps.BusinessLogic.LatLong;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by aliTang on 10/28/15.
 */
public class GpsHelper {

    public static GpsHelper gpsHelperObj = null;

    public static GpsHelper make() {
        if(gpsHelperObj != null)
            return gpsHelperObj;
        return new GpsHelper();
    }

    public Intent GpsEnableRequest(){
        return new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    }

    public boolean IsEnabledGps(Context context){
        LocationManager locMgr = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        return locMgr.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    enum NSEW{N, S, E, W}

    public String DecToDegree(double gpsValue, Boolean isLat){
        DecimalFormat df = new DecimalFormat("0.##");
        String cardinal_point;
        int degree;
        int min;
        String sec;

        if(gpsValue < 0){
            if(isLat)
                cardinal_point = NSEW.S.toString();
            else
                cardinal_point = NSEW.W.toString();
            gpsValue *= -1;
        }
        else
        if (isLat)
            cardinal_point = NSEW.N.toString();
        else
            cardinal_point = NSEW.E.toString();

         degree = (int)gpsValue;
         min = (int)((gpsValue - degree)*60);

        //One degree (°) is equal to 60 minutes (') and equal to 3600 seconds ("):
         sec = df.format((gpsValue - degree - (min/60.0))*3600);


        return  degree+"° "+min+"' "+sec+"\" "+cardinal_point;

    }

    public String GetDate(long dDate){
        Date dt = new Date(dDate);
        SimpleDateFormat sdf = new  SimpleDateFormat("HH:mm");
        return sdf.format(dDate).toString();
    }

    public boolean IsEnabledNetworkProvider(Context context){
        LocationManager locMgr = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        return locMgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
}
