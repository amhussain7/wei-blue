package com.btgps.alitang.btgps.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.btgps.alitang.btgps.Activities.NavigationDrawer.NavigationDrawerFragment;
import com.btgps.alitang.btgps.Helper.HelperMethods;
import com.btgps.alitang.btgps.BusinessLogic.Bluetooth;
import com.btgps.alitang.btgps.R;


public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private static final int REQUEST_ENABLE_BT = 3;
    private static final String TAG = "SplashActivity";
    //public boolean btEnabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_splash__screen);
        //ImageView img = (ImageView)this.findViewById(R.id.splash_img);

        //mNavigationDrawerFragment = (NavigationDrawerFragment)
        //        getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        IsBtSupported();
        startBluetooth();

        // Set up the drawer.
        //mNavigationDrawerFragment.setUp(
        //       R.id.navigation_drawer,
        //        (DrawerLayout) findViewById(R.id.drawer_layout));

        //new splash_screen().execute(img);
      //  btnTransmit = (Button)findViewById(R.id.btnTx);
       // btnTransmit.setOnClickListener(this);

    }

    public void IsBtSupported(){
        if(!Bluetooth.make().IsDeviceSupported()){
            new HelperMethods().ShowMsg(this.getApplicationContext(), "not supported", Toast.LENGTH_LONG);
            this.finish();
            System.exit(0);
        }
    }

    public void startBluetooth() {

        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!Bluetooth.make().IsEnabled()) {

            startActivityForResult(Bluetooth.make().BtEnableRequest(), REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } //else if (mChatService == null) {
        else {
            //strat home activity thread
            CallMainActivity();
        }

        //}
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        switch (requestCode){
            case REQUEST_ENABLE_BT:
                Log.d(TAG, "Bluetooth Enable Request");
                if(resultCode == RESULT_OK)
                    CallMainActivity();
                else
                    this.finish();
                break;
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       /* if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }*/
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void CallMainActivity(){
        Thread sleepThread = new Thread(){
            public void run(){
                try {
                    // Thread will sleep for 5 seconds
                    sleep(3*1000);

                    // After 5 seconds redirect to another intent
                    Intent intHome=new Intent(getBaseContext(), home_activity.class);
                    startActivity(intHome);

                    //Remove activity
                    finish();

                } catch (Exception e) {

                }
            }
        };
        // start thread
        sleepThread.start();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
           // View utmLayout = rootView.findViewById(R.id.lyUtm);
            //TextView utm = (TextView)utmLayout.findViewById(R.id.txtGps);
            //utm.setText("UTM ");
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
