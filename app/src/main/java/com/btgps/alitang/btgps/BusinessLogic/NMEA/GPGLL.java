package com.btgps.alitang.btgps.BusinessLogic.NMEA;

/**
 * Created by aliTang on 11/4/15.
 */
 /*
     $GPGLL,4916.45,N,12311.12,W,225444,A,*1D
    Where:
     GLL          Geographic position, Latitude and Longitude
     4916.46,N    Latitude 49 deg. 16.45 min. North
     12311.12,W   Longitude 123 deg. 11.12 min. West
     225444       Fix taken at 22:54:44 UTC
     A            Data Active or V (void)
     *iD          checksum data
     */
public class GPGLL {
    //variable decleration
    String nmeaType;
    String latitude;
    char NS;
    String longitude;
    char EW;
    String lastUpdated;
    String dataValid;
    String checkSum;

    //properties and getter and setters

    String get_nmeaType() { return nmeaType; }
    void set_nmeaType(String nmeaType) { this.nmeaType = nmeaType; }

    String get_latitude() { return latitude; }
    void set_latitude(String latitude) { this.latitude = latitude; }

    char get_NS() { return NS; }
    void set_NS(char NS) { this.NS = NS; }

    String get_longitude() { return longitude; }
    void set_longitude(String longitude) { this.longitude = longitude; }

    char get_EW() { return EW; }
    void set_EW(char EW) { this.EW = EW; }

    String get_lastUpdated() { return lastUpdated; }
    void set_lastUpdated(String lastUpdated) { this.lastUpdated = lastUpdated; }

    String get_dataValid() { return dataValid; }
    void set_dataValid(String dataValid) { this.dataValid = dataValid; }

    String get_checkSum() { return checkSum; }
    void set_checkSum(String checkSum) { this.checkSum = checkSum; }

    //singleton class 
    public GPGLL()
    {

    }
    private static GPGLL gpgll = null;

    public static GPGLL make_Gpggs(String[] receivedData)
    {
        if(gpgll == null)
            gpgll = new GPGLL();
        gpgll.Init_variables();
        gpgll.dataAssign(receivedData);
        return gpgll;
    }

    private void Init_variables()
    {

        latitude = "";
        NS = 'X';
        longitude = "";
        EW = 'X';
        lastUpdated = "";
        dataValid = "";
        checkSum = "";
    }

    /**
     * @param receivedData
     * @return
     */
    private boolean dataAssign(String[] receivedData)
    {
        //if array is null return false
        if (receivedData == null)
            return false;

        //$GPGLL,4916.45,N,12311.12,W,225444,A,*1D
        //start setting the values from the array
        set_nmeaType(receivedData[0]);
        set_latitude(receivedData[1]);
        if(receivedData[2] != "")
            set_NS(receivedData[2].toCharArray()[2]);
        set_longitude(receivedData[3]);
        if(receivedData[4] != "")
            set_EW(receivedData[4].toCharArray()[0]);
        set_lastUpdated(receivedData[5]);
        set_checkSum(receivedData[7]);
        return true;
    }
}
