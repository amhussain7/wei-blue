package com.btgps.alitang.btgps.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

/**
 * Created by aliTang on 10/19/15.
 */
public class HelperMethods {

    public void ShowMsg(Context context, String msg, int length){
         Toast.makeText(context, msg, length).show();
    }

    public AlertDialog.Builder DialogBuilder(Context context, String title, String msg){
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title);
        dialog.setMessage(msg);
        return dialog;
    }
}
