package com.btgps.alitang.btgps.Activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.btgps.alitang.btgps.BusinessLogic.Bluetooth;
import com.btgps.alitang.btgps.R;

import java.util.Set;

public class BT_List extends AppCompatActivity implements OnClickListener {

    private static final String TAG = "HomeActivity";
    Button btnScanBTs = null;
    ArrayAdapter<String> ADPairedTBTList = null;
    ArrayAdapter<String> ADNewBTList = null;
    ListView lvPairedBluetooth = null;
    ListView lvNewBluetooth = null;

    /**
     * Return Intent extra
     */
    public static String EXTRA_DEVICE_ADDRESS = "device_address";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //wait cursor request
        //requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.btdevicelist);

        //set the result canceled if user clicks back
        setResult(Activity.RESULT_CANCELED);

        //initilaize actions and set adaptor
        SetArrayAdapt();

        //reg broadcast discovery
        this.registerReceiver(mReceiver, Bluetooth.make().RegDeviceDiscovery());
        this.registerReceiver(mReceiver, Bluetooth.make().RegDeviceDiscoveryFinish());


    }

    public void SetArrayAdapt() {

        btnScanBTs = (Button) findViewById(R.id.btnBTSearch);
        btnScanBTs.setOnClickListener(this);

        lvPairedBluetooth = (ListView) findViewById(R.id.lvBTKnownPeers);
        lvNewBluetooth = (ListView) findViewById(R.id.lvNewPeers);
        lvPairedBluetooth.setOnItemClickListener(mBTDeviceListener);
        lvNewBluetooth.setOnItemClickListener(mBTDeviceListener);

        ADPairedTBTList = new ArrayAdapter<String>(this, R.layout.activity_bt__list);
        ADNewBTList = new ArrayAdapter<String>(this, R.layout.activity_bt__list);

        lvPairedBluetooth.setAdapter(ADPairedTBTList);
        lvNewBluetooth.setAdapter(ADNewBTList);

        AddDevices(ADPairedTBTList, Bluetooth.make().GetBoundedDevices());
    }

    public void AddDevices(ArrayAdapter<String> arrayAdapter, Set<BluetoothDevice> pairedDevices) {
        if (pairedDevices.size() > 0) {
            //findViewById(R.id.)
            for (BluetoothDevice device : pairedDevices) {
                arrayAdapter.add(device.getName() + "\t" + device.getAddress());
            }
        } else {
            String noDevices = "No Devices";
            arrayAdapter.add(noDevices);
        }
    }

    public void SearchNewDevices() {
        Log.d(TAG, "doDiscovery()");

        // Indicate scanning in the title
        //setProgressBarIndeterminateVisibility(true);
        //setTitle(R.string.scanning);

        // Turn on sub-title for new devices
        // findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);

        // If we're already discovering, stop it
        Bluetooth.make().CancelDiscovery();

        // Request discover from BluetoothAdapter
        Bluetooth.make().SearchDevice();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Make sure we're not doing discovery anymore
       Bluetooth.make().CancelDiscovery();

        // Unregister broadcast listeners
        this.unregisterReceiver(mReceiver);
    }

    /**
     * The BroadcastReceiver that listens for discovered devices and changes the title when
     * discovery is finished
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {

                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = Bluetooth.make().DeviceFound(intent);

                // If it's already paired, skip it, because it's been listed already
                if (device != null)
                    ADNewBTList.add(device.getName() + "\t" + device.getAddress());

                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setProgressBarIndeterminateVisibility(false);
                //setTitle(R.string.select_device);
                if (ADNewBTList.getCount() == 0) {
                    String noDevices = "No Devices";
                    ADNewBTList.add(noDevices);
                }
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bt__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBTSearch:
                SearchNewDevices();
                break;

            case R.id.lvBTKnownPeers:

                break;
        }
    }

    private AdapterView.OnItemClickListener mBTDeviceListener =
            new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    //stop discovering the new BT devices
                    Bluetooth.make().CancelDiscovery();

                    String macAddress = ((TextView) view).getText().toString().split("\t")[1];

                    //generate result intent to return the result
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_DEVICE_ADDRESS, macAddress);

                    //set result and return to calling activity
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            };
}