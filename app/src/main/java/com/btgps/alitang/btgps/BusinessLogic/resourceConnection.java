package com.btgps.alitang.btgps.BusinessLogic;

/**
 * Created by Wei Tang on 2015-09-13.
 */
public interface resourceConnection<T> {
   public boolean connect();
}
