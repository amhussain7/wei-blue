package com.btgps.alitang.btgps.BusinessLogic.NMEA;

/**
 * Created by aliTang on 11/4/15.
 */
 /*
     * $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
    Where:
     GGA          Global Positioning System Fix Data
     123519       Fix taken at 12:35:19 UTC
     4807.038,N   Latitude 48 deg 07.038' N
     01131.000,E  Longitude 11 deg 31.000' E
     1            Fix quality: 0 = invalid
                               1 = GPS fix (SPS)
                               2 = DGPS fix
                               3 = PPS fix
			       4 = Real Time Kinematic
			       5 = Float RTK
                               6 = estimated (dead reckoning) (2.3 feature)
			       7 = Manual input mode
			       8 = Simulation mode
     08           Number of satellites being tracked
     0.9          Horizontal dilution of position
     545.4,M      Altitude, Meters, above mean sea level
     46.9,M       Height of geoid (mean sea level) above WGS84
                      ellipsoid
     (empty field) time in seconds since last DGPS update
     (empty field) DGPS station ID number
     *47          the checksum data, always begins with *
     */
public class GPGGA {
    //variable decleration
    String nmeaType;
    String lastUpdated;
    String latitude;
    char NS;
    String longitude;
    char EW;
    short fixedQuality;
    short satCount;
    String hDOP;
    String altitudeInMeters;
    char altitudeScale;
    String ellipsoidInMeters;
    char ellipsoidScale;
    String timeLastUpdated;
    String wId;
    String checkSum;

    //properties and getter and setters

    String Fields_Gpgga[] = new String[]{"NMEA TYPE", "LAST UPDATED"
            , "LATITUDE", "LONGITUDE", "FIXED QUALITY"
            ,"SATELLITE COUNT", "H - DILUTION OF PRECISION"
            , "ALTITUDE", "ELLIPSOID", "TIME LAST UPDATED"};

    String get_nmeaType() { return nmeaType; }
    void set_nmeaType(String nmeaType) { this.nmeaType = nmeaType; }

    String get_lastUpdated() { return lastUpdated; }
    void set_lastUpdated(String lastUpdated) { this.lastUpdated = lastUpdated; }

    String get_latitude() { return latitude; }
    void set_latitude(String latitude) { this.latitude = latitude; }

    char get_EW() { return EW; }
    void set_EW(char EW) { this.EW = EW; }

    String get_longitude() { return longitude; }
    void set_longitude(String longitude) { this.longitude = longitude; }

    char get_NS() { return NS; }
    void set_NS(char NS) { this.NS = NS; }

    short get_fixedQuality() { return fixedQuality; }
    void set_fixedQuality(short fixedQuality) { this.fixedQuality = fixedQuality; }

    short get_satCount() { return satCount; }
    void set_satCount(short satCount) { this.satCount = satCount; }

    String get_hDOP() { return hDOP; }
    void set_hDOP(String hDOP) { this.hDOP = hDOP; }

    String get_altitudeInMeters() { return altitudeInMeters; }
    void set_altitudeInMeters(String altitudeInMeters) { this.altitudeInMeters = altitudeInMeters; }

    char get_altitudeScale() { return altitudeScale; }
    void set_altitudeScale(char altitudeScale) { this.altitudeScale = altitudeScale; }

    String get_ellipsoidInMeters() { return ellipsoidInMeters; }
    void set_ellipsoidInMeters(String ellipsoidInMeters) { this.ellipsoidInMeters = ellipsoidInMeters; }

    char get_ellipsoidScale() { return ellipsoidScale; }
    void set_ellipsoidScale(char ellipsoidScale) { this.ellipsoidScale = ellipsoidScale; }

    String get_timeLastUpdated() { return timeLastUpdated; }
    void set_timeLastUpdated(String timeLastUpdated) { this.timeLastUpdated = timeLastUpdated; }

    String get_wId() { return wId; }
    void set_wId(String wId) { this.wId = wId; }

    String get_checkSum() { return checkSum; }
    void set_checkSum(String checkSum) { this.checkSum = checkSum; }

    //singleton class 
    public GPGGA() {

    }
    private static GPGGA gpgga = null;

    public static GPGGA make_Gpggs(String[] receivedData)
    {
        if(gpgga == null)
            gpgga = new GPGGA();
        gpgga.Init_variables();
        gpgga.dataAssign(receivedData);
        return gpgga;
    }

    private void Init_variables()
    {
        lastUpdated = "";
        latitude = "";
        EW = 'X';
        longitude = "";
        NS = 'X';
        fixedQuality = 0;
        satCount = 0;
        hDOP = "";
        altitudeInMeters = "";
        altitudeScale = 'X';
        ellipsoidInMeters = "";
        ellipsoidScale = 'X';
        timeLastUpdated = "";
        checkSum = "";
    }

    private boolean dataAssign(String[] receivedData)
    {
        //if array is null return false
        if (receivedData == null)
            return false;

        //$GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4, M, 46.9,  M  ,  ,*47
        //   0     1      2       3  4        5 6  7  8    9   10  11  12 13 14
        //start setting the values from the array
        set_nmeaType(receivedData[0]);
        set_lastUpdated(receivedData[1]);
        set_latitude(receivedData[2]);
        if(receivedData[3] != "")
            set_NS(receivedData[3].toCharArray()[0]);
        set_longitude(receivedData[4]);
        if(receivedData[5] != "")
            set_EW(receivedData[5].toCharArray()[0]);
        if(receivedData[6] != "")
            set_fixedQuality(Short.parseShort(receivedData[6]));
        if(receivedData[7] != "")
            set_satCount(Short.parseShort(receivedData[7]));
        set_hDOP(receivedData[8]);
        set_altitudeInMeters(receivedData[9]);
        if (receivedData[10] != "")
            set_altitudeScale(receivedData[10].toCharArray()[0]);
        set_ellipsoidInMeters(receivedData[11]);
        if (receivedData[12] != "")
            set_ellipsoidScale(receivedData[10].toCharArray()[0]);
        String[] widNcksum = receivedData[14].split("\\*");
        if (widNcksum.length > 2)
        {
            set_wId(widNcksum[0]);
            set_checkSum('*' + widNcksum[1]);
        }
        return true;
    }

    public String SeralizeGPGGA(
            String nmeaType,
            String lastUpdated,
            String latitude,
            char NS,
            String longitude,
            char EW,
            short fixedQuality,
            short satCount,
            String hDOP,
            String altitudeInMeters,
            char altitudeScale,
            String ellipsoidInMeters,
            char ellipsoidScale,
            String timeLastUpdated,
            String wId,
            String checkSum){
        this.nmeaType = nmeaType;
        this.lastUpdated = lastUpdated;
        this.latitude = latitude;
        this.NS = NS;
        this.longitude = longitude;
        this.EW = EW;
        this.fixedQuality = fixedQuality;
        this.satCount = satCount;
        this.hDOP = hDOP;
        this.altitudeInMeters = altitudeInMeters;
        this.altitudeScale = altitudeScale;
        this.ellipsoidInMeters = ellipsoidInMeters;
        this.ellipsoidScale = ellipsoidScale;
        this.timeLastUpdated = timeLastUpdated;
        this.wId = wId;
        this.checkSum = checkSum;
       return StrGpgga();
    }

    public String StrGpgga(){
        return "GPGGA"+"";
    }

    public void SerializeGPGGA(){

    }


}