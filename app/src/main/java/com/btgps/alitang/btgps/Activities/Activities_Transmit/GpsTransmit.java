package com.btgps.alitang.btgps.Activities.Activities_Transmit;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


//import com.btgps.alitang.btgps.BusinessLogic.BTServerFiles.BTServer;
import com.btgps.alitang.btgps.Activities.BT_List;
import com.btgps.alitang.btgps.Activities.NavigationDrawer.NavigationDrawerFragment;
import com.btgps.alitang.btgps.Activities.NavigationDrawer.PlaceholderFragment;
import com.btgps.alitang.btgps.BusinessLogic.BTServerFiles.BlueServer;
import com.btgps.alitang.btgps.BusinessLogic.Bluetooth;
import com.btgps.alitang.btgps.BusinessLogic.LatLong.GpsHelper;
import com.btgps.alitang.btgps.BusinessLogic.LatLong.PositionListener;
import com.btgps.alitang.btgps.BusinessLogic.LatLong.Utm;
import com.btgps.alitang.btgps.R;

public class GpsTransmit extends AppCompatActivity implements View.OnClickListener,
        NavigationDrawerFragment.NavigationDrawerCallbacks {
    /**
     * Member object for the chat services
     */
    private BlueServer sService = null;
    private static final int REQUEST_ENABLE_GPS = 5;
    /**
     * String buffer for outgoing messages
     */
    private StringBuffer mOutStringBuffer;

    private static final String TAG = "BluetoothTransmit";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    //private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;

    Button btnTramsmit = null;
    Button btnReceive = null;
    TextView txtCount = null;
    PositionListener locationData = null;
    TextView txtDms = null;
    TextView txtDecimal = null;
    TextView txtUtm = null;
    TextView txtLat = null;
    TextView txtLng = null;
    TextView txtDegLat = null;
    TextView txtDegLng = null;
    TextView txtUtmLat = null;
    TextView txtUtmLong = null;
    TextView txtUtmSubLat = null;
    TextView txtUtmSubLong = null;
    TextView txtDmsSubLat = null;
    TextView txtDmsSubLong = null;

    GpsHelper gpsHelper = null;
    Utm utm = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tx);
        overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_in_top);

        View rootView = findViewById(R.id.Utm);
        View coutnView = findViewById(R.id.deviceCountUtm);
        View viewDecimal = findViewById(R.id.viewDecimal);
        View viewDms = findViewById(R.id.viewDms);
        View viewUtm = findViewById(R.id.viewUtm);

        viewDecimal.setOnClickListener(this);
        viewDms.setOnClickListener(this);
        viewUtm.setOnClickListener(this);
//        Typeface typeFace= Typeface.createFromAsset(getAssets(), "fonts/instruction.ttf");
        txtLat = (TextView)viewDecimal.findViewById(R.id.txtLat);
  //      txtLat.setTypeface(typeFace);
        txtLng = (TextView)viewDecimal.findViewById(R.id.txtLong);
        txtDecimal = (TextView)viewDecimal.findViewById(R.id.txtGps);

        txtDms = (TextView)viewDms.findViewById(R.id.txtGps);
        txtDegLat = (TextView)viewDms.findViewById(R.id.txtLat);
        txtDegLng = (TextView)viewDms.findViewById(R.id.txtLong);
        txtUtmSubLat = (TextView)viewDms.findViewById(R.id.txtsubLat);
        txtUtmSubLong = (TextView)viewDms.findViewById(R.id.txtsublong);

        txtCount = (TextView)coutnView.findViewById(R.id.txtCount);

        txtUtmLat = (TextView)viewUtm.findViewById(R.id.txtLat);
        txtUtmLong = (TextView)viewUtm.findViewById(R.id.txtLong);
        txtUtm = (TextView)viewUtm.findViewById(R.id.txtGps);
        txtDmsSubLat = (TextView)viewUtm.findViewById(R.id.txtsubLat);
        txtDmsSubLong = (TextView)viewUtm.findViewById(R.id.txtsublong);

        txtDms.setText("DMS");
        txtDecimal.setText("DEC");
        txtUtm.setText("UTM");
        txtUtmSubLat.setText("Degree, Minutes, Seconds Lat");
        txtUtmSubLong.setText("Degree, Minutes, Seconds Long");
        txtDmsSubLat.setText("Northward Measured Distance");
        txtDmsSubLong.setText("Eastward Measured Distance");

        btnTramsmit = (Button)rootView.findViewById(R.id.btnTx);
        btnReceive = (Button)rootView.findViewById(R.id.btnRx);

        btnTramsmit.setOnClickListener(this);
        btnReceive.setOnClickListener(this);
        locationData = new PositionListener(this);

        EnableGps();
        gpsHelper = new GpsHelper();
        utm = new Utm();

      //  mNavigationDrawerFragment = (NavigationDrawerFragment)
        //        getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        //mTitle = getTitle();

        // Set up the drawer.
        //mNavigationDrawerFragment.setUp(
          //      R.id.navigation_drawer,
            //    (DrawerLayout) findViewById(R.id.drawer_layout));
        //locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
    }

   /* final Handler handler = new Handler();
    private void Testdata(){

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sendMessage(Math.random()*100+" This is sample text");
            }
        }, 20000);
    }*/

    public void EnableGps() {

        // If Gps is not on, request that it be enabled.
        // Transmit will then be called during onActivityResult
        if (!GpsHelper.make().IsEnabledGps(this))
            startActivityForResult(GpsHelper.make().GpsEnableRequest(), REQUEST_ENABLE_GPS);
    }

    private void setupServer() {
        Log.d(TAG, "setupServer");

        // Initialize the BluetoothChatService to perform bluetooth connections
        sService = new BlueServer(this, sHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");
        FindDevice();
    }

    /**
     * The Handler that gets information back from the BluetoothChatService
     */
    private final Handler sHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //FragmentActivity activity = this;
            switch (msg.what) {
                case Bluetooth.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BlueServer.STATE_CONNECTED:
                            //setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
                            //mConversationArrayAdapter.clear();
                            break;
                        case BlueServer.STATE_CONNECTING:
                            //setStatus(R.string.title_connecting);
                            break;
                        case BlueServer.STATE_LISTEN:
                        case BlueServer.STATE_NONE:
                            //setStatus(R.string.title_not_connected);
                            break;
                    }
                    break;
                case Bluetooth.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    //mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case Bluetooth.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    //mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
                    txtCount.setText("" + ":  " + readMessage);
                    break;
                case Bluetooth.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                   // mConnectedDeviceName = msg.getData().getString(Bluetooth.DEVICE_NAME);
                    //if (null != activity) {
                        //Toast.makeText(activity, "Connected to "
                         //       + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    //}
                    break;
                case Bluetooth.MESSAGE_TOAST:
                    if (null != this) {
                        //Toast.makeText(GpsTransmit.class, msg.getData().getString(Bluetooth.TOAST),
                          //      Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_tx, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
         if ( sService == null) {
            setupServer();
        }
    }

    private void FindDevice(){
        Intent intentpd = new Intent(this, BT_List.class);
        startActivityForResult(intentpd, REQUEST_CONNECT_DEVICE_SECURE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras().getString(BT_List.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = Bluetooth.make().btAdaptor.getRemoteDevice(address);
        // Attempt to connect to the device
        sService.connect(device, secure);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        switch (requestCode){
            case REQUEST_CONNECT_DEVICE_SECURE:
                if(resultCode == Activity.RESULT_OK){
                    String deviceMac = intent.getExtras().getString(BT_List.EXTRA_DEVICE_ADDRESS);
                    connectDevice(intent, true);
                }
                break;
            case REQUEST_ENABLE_GPS:
                if(resultCode == Activity.RESULT_CANCELED) {
                    //new HelperMethods().DialogBuilder(this, "Alert", "App need GPS");
                    this.finish();
                }
            case REQUEST_CONNECT_DEVICE_INSECURE:
                break;
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (sService.getState() != BlueServer.STATE_CONNECTED) {
            //Toast.makeText(getActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            sService.write(send);

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
            //mOutEditText.setText(mOutStringBuffer);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnTx:
                sendMessage(Math.random() * 100 + " This is sample text");
                break;
            case R.id.btnRx:
                Location loc = locationData.GetLocation(this);
                double tmpLat = loc.getLatitude();
                double tmpLon = loc.getLongitude();
                txtLat.setText(tmpLat + "");
                txtLng.setText(tmpLon + "");
                txtCount.setText(gpsHelper.GetDate(loc.getTime()));
                txtDegLat.setText(gpsHelper.DecToDegree(tmpLat, true));
                txtDegLng.setText(gpsHelper.DecToDegree(tmpLon, false));
                String[] tempUtm = utm.ToUTM(tmpLat, tmpLon);
                txtUtmLat.setText(tempUtm[0]);
                txtUtmLong.setText(tempUtm[1]);
                break;
            case R.id.viewDecimal:
                //mNavigationDrawerFragment.OpenDrawer();
                break;
            case R.id.viewDms:
                //mNavigationDrawerFragment.OpenDrawer();
                break;
            case R.id.viewUtm:
                //mNavigationDrawerFragment.OpenDrawer();
                break;
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.make().newInstance(position + 1))
                .commit();
    }


}
