package com.btgps.alitang.btgps.BusinessLogic.LatLong;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by aliTang on 10/28/15.
 */
public class PositionListener extends Service implements LocationListener {

    Location blocation;
    protected LocationManager locationManager = null;
    private static final String TAG = "POSITION LISTENER";
    private static final int LOCATION_INTERVAL = 10000;
    private static final float LOCATION_DISTANCE = 0f;

    public PositionListener(Context ctx) {
        locationManager = (LocationManager) ctx
                .getSystemService(LOCATION_SERVICE);
        GetLocation(ctx);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(TAG, "onLocationChanged: " + location);
        blocation.set(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Log.e(TAG, "onStatusChanged: " + s);
    }

    @Override
    public void onProviderEnabled(String s) {
        Log.e(TAG, "onProviderEnabled: " + s);
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.e(TAG, "onProviderDisabled: " + s);

    }

    public Location GetLocation(Context ctx){
        locationManager.requestLocationUpdates
                (LocationManager.NETWORK_PROVIDER,LOCATION_INTERVAL, LOCATION_DISTANCE, this);
        if(locationManager != null)
            blocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (blocation != null){
            return  blocation;
        }
        return  null;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
