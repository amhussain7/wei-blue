package com.btgps.alitang.btgps.BusinessLogic.NMEA;

/**
 * Created by aliTang on 11/4/15.
 */
/*
     * $GPBWC,225444,4917.24,N,12309.57,W,051.9,T,031.6,M,001.3,N,004*29
    where:
        BWC          Bearing and distance to waypoint - great circle
        225444       UTC time of fix 22:54:44
        4917.24,N    Latitude of waypoint
        12309.57,W   Longitude of waypoint
        051.9,T      Bearing to waypoint, degrees true
        031.6,M      Bearing to waypoint, degrees magnetic
        001.3,N      Distance to waypoint, Nautical miles
        004          Waypoint ID
        *29          checksum
     */
public class GPBWC {
    //variable decleration
    String nmeaType;
    String lastUpdated;
    String latitude;
    char NS;
    String longitude;
    char EW;
    String tWayPoint;
    char T;
    String mWayPoint;
    char M;
    String nWayPoint;
    char N;
    String wId;
    String checkSum;

    //properties and getter and setters

    String get_nmeaType() { return nmeaType; }
    void set_nmeaType(String nmeaType) { this.nmeaType = nmeaType; }

    String get_lastUpdated() { return lastUpdated; }
    void set_lastUpdated(String lastUpdated) { this.lastUpdated = lastUpdated; }

    String get_latitude() { return latitude; }
    void set_latitude(String latitude) { this.latitude = latitude; }

    char get_NS() { return NS; }
    void set_NS(char NS) { this.NS = NS; }

    String get_longitude() { return longitude; }
    void set_longitude(String longitude) { this.longitude = longitude; }

    char get_EW() { return EW; }
    void set_EW(char EW) { this.EW = EW; }

    String get_tWayPoint() { return tWayPoint; }
    void set_tWayPoint(String tWayPoint) { this.tWayPoint = tWayPoint; }

    char get_T() { return T; }
    void set_T(char T) { this.T = T; }

    String get_mWayPoint() { return mWayPoint; }
    void set_mWaypoint(String mWaypoint) { this.mWayPoint = mWaypoint; }

    char get_M() { return M; }
    void set_M(char M) { this.M = M; }

    String get_nWayPoint() { return nWayPoint; }
    void set_nWaypoint(String nWaypoint) { this.nWayPoint = nWaypoint; }

    char get_N() { return N; }
    void set_N(char N) { this.N = N; }

    String get_wId() { return wId; }
    void set_wId(String wId) { this.wId = wId; }

    String get_checkSum() { return checkSum; }
    void set_checkSum(String checkSum) { this.checkSum = checkSum; }

    //singleton class 
    public GPBWC() {

    }
    private static GPBWC gpbwc = null;

    public static GPBWC make_Gpbwc(String[] receivedData)
    {
        if (gpbwc == null)
            gpbwc = new GPBWC();
        gpbwc.Init_variables();
        gpbwc.dataAssign(receivedData);
        return gpbwc;
    }

    private void Init_variables()
    {
        nmeaType = "";
        lastUpdated = "";
        latitude = "";
        NS = 'X';
        longitude = "";
        EW = 'X';
        tWayPoint = "";
        T = 'X';
        mWayPoint = "";
        M = 'X';
        nWayPoint = "";
        N = 'X';
        wId = "";
        checkSum = "";
    }

    private boolean dataAssign(String[] receivedData)
    {
        //if array is null return false
        if (receivedData == null)
            return false;

        if (receivedData.length < 13)
            return false;
        // $GPBWC,225444,4917.24,N,12309.57,W,051.9,T,031.6,M,001.3,N,004*29
        //start setting the values from the array
        set_nmeaType(receivedData[0]);
        set_lastUpdated(receivedData[1]);
        set_latitude(receivedData[2]);
        if(receivedData[3] != "")
            set_NS(receivedData[3].toCharArray()[0]);
        set_longitude(receivedData[4]);
        if(receivedData[5] != "")
            set_EW(receivedData[5].toCharArray()[0]);
        set_tWayPoint(receivedData[6]);
        if (receivedData[7] != "")
            set_T(receivedData[7].toCharArray()[0]);
        set_mWaypoint(receivedData[8]);
        if (receivedData[9] != "")
            set_EW(receivedData[9].toCharArray()[0]);
        set_nWaypoint(receivedData[10]);
        if (receivedData[11] != "")
            set_N(receivedData[11].toCharArray()[11]);
        String[] widNcksum = receivedData[12].split("\\*");
        if (widNcksum.length > 2)
        {
            set_wId(widNcksum[0]);
            set_checkSum('*' + widNcksum[1]);
        }
        return true;
    }

}

