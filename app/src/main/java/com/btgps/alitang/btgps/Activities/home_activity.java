package com.btgps.alitang.btgps.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.btgps.alitang.btgps.Activities.Activities_Transmit.GpsTransmit;
import com.btgps.alitang.btgps.BusinessLogic.Bluetooth;
import com.btgps.alitang.btgps.R;

public class home_activity extends AppCompatActivity implements OnClickListener {

    /*
   * Buttons declerations
   */

    private static final String TAG = "HomeActivity";
    private Button btnTransmit = null;
    private Button btnDevices = null;
    private Button btnReceive = null;

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        btnTransmit = (Button)findViewById(R.id.btnTx);
        btnDevices = (Button)findViewById(R.id.btnPairedDevices);
        btnDevices.setOnClickListener(this);
        btnTransmit.setOnClickListener(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        switch (requestCode){
            case REQUEST_CONNECT_DEVICE_SECURE:
                if(resultCode == Activity.RESULT_OK){
                    String deviceMac = intent.getExtras().getString(BT_List.EXTRA_DEVICE_ADDRESS);
                    Bluetooth.make().ConnectDevice(deviceMac, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnTx:
                Intent intentTx = new Intent(this, GpsTransmit.class);
                startActivityForResult(intentTx, REQUEST_CONNECT_DEVICE_SECURE);
                break;
            case R.id.btnPairedDevices:
                Intent intentpd = new Intent(this, BT_List.class);
                startActivityForResult(intentpd, REQUEST_CONNECT_DEVICE_SECURE);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
