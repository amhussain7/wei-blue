package com.btgps.alitang.btgps.Activities;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.btgps.alitang.btgps.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class Splash_ScreenFragment extends Fragment {

    public Splash_ScreenFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_splash__screen, container, false);
    }
}
