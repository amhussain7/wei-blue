package com.btgps.alitang.btgps.BusinessLogic.BTServerFiles;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import com.btgps.alitang.btgps.BusinessLogic.Bluetooth;

import java.io.IOException;
import java.util.UUID;


/**
 * Created by aliTang on 10/21/15.
 */
public class ConnectBTThread {
    /*private BluetoothSocket BTSocket;
    private BluetoothDevice BTDevice;
    private String connSocketType;
    private static final String TAG = "BTConnectThread";


    public ConnectBTThread(Handler sHandler, BluetoothDevice btDevice, boolean isSecure){

        BTDevice = btDevice;

        //get a BT Socket for a presented device
        if(isSecure){
            connSocketType = "Secure";
            GetSetBTSocket(SECURE_UUID);}
        else{
            connSocketType = "InSecure";
            GetSetBTSocket(INSURE_UUID);}

    }

    public void GetSetBTSocket(UUID uuid){
        BluetoothSocket BTSocketTmp = null;
        try {
            BTSocketTmp = BTDevice.createInsecureRfcommSocketToServiceRecord(SECURE_UUID);
        } catch (IOException ex){
            Log.e(TAG, "Socket Type: " + connSocketType + "create() failed", ex);
        }
        BTSocket = BTSocketTmp;
    }

    @Override
    public void run() {
        Log.d(TAG, "Socket Type: " + connSocketType + "BEGIN mAcceptThread" + this);
        // setName("AcceptThread" + connSocketType);

        //its is import to cancel
        Bluetooth.make().btAdaptor.cancelDiscovery();

        //to to create a connection to the bluetoothSocket
        try{
            BTSocket.connect();
        }catch (IOException ex){
            try {
                //exception occured, please close the connection
                BTSocket.close();
            }catch (IOException ex2){
                Log.e(TAG, "unable to close() " + connSocketType +
                        " socket during connection failure", ex2);
            }
            //write a message for attempted connection failed
            ResetServer("BTConnFailed: Unable to connect device");

            //
            return;
        }
        // Reset the ConnectThread because we're done
        synchronized (BTServer.class) {
             connectBTThread = null;
        }

        // Start the connected thread
        Connected(BTSocket, connSocketType);
    }

    public void cancel() {
        Log.d(TAG, "Socket Type" + connSocketType + "cancel " + this);
        try {
            BTSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Socket Type" + connSocketType + "close() of server failed", e);
        }
    }*/
}